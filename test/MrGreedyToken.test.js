const { assert } = require("chai");
const { accounts } = require("../scripts/utils/utils");
const truffleAssert = require("truffle-assertions");
const MrGreedyToken = artifacts.require("MrGreedyToken");

describe("MrGreedyToken", () => {
  let OWNER;
  let DUDE;
  let TREASURY;

  let mrGreedyToken;

  const oneToken = 10 ** 6;

  before("setup", async () => {
    OWNER = await accounts(0);
    DUDE = await accounts(1);
    TREASURY = await accounts(2);
  });

  describe("constructor", () => {
    it("should set all variables", async () => {
      mrGreedyToken = await MrGreedyToken.new(TREASURY);

      assert.equal(await mrGreedyToken.treasury(), TREASURY);
    });
  });

  describe("decimals()", () => {
    beforeEach("setup", async () => {
      mrGreedyToken = await MrGreedyToken.new(TREASURY);
    });

    it("should successfully return number of decimals", async () => {
      assert.equal(await mrGreedyToken.decimals(), "6");
    });
  });

  describe("getResultingTransferAmount()", () => {
    beforeEach("setup", async () => {
      mrGreedyToken = await MrGreedyToken.new(TREASURY);
    });

    it("should successfully return resulting amount", async () => {
      assert.equal(await mrGreedyToken.getResultingTransferAmount(10 * oneToken), 0);
      assert.equal(await mrGreedyToken.getResultingTransferAmount(10 * oneToken + 1), 1);
    });
  });

  describe("transfer()", () => {
    beforeEach("setup", async () => {
      mrGreedyToken = await MrGreedyToken.new(TREASURY);

      await mrGreedyToken.mint(OWNER, 100 * oneToken);
    });

    it("should successfully transfer tokens", async () => {
      await mrGreedyToken.transfer(DUDE, 7 * oneToken);

      assert.equal(await mrGreedyToken.balanceOf(OWNER), 93 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(DUDE), 0);
      assert.equal(await mrGreedyToken.balanceOf(TREASURY), 7 * oneToken);

      await mrGreedyToken.transfer(DUDE, 93 * oneToken);

      assert.equal(await mrGreedyToken.balanceOf(OWNER), 0 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(DUDE), 83 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(TREASURY), 17 * oneToken);
    });
  });

  describe("transferFrom()", () => {
    beforeEach("setup", async () => {
      mrGreedyToken = await MrGreedyToken.new(TREASURY);

      await mrGreedyToken.mint(OWNER, 100 * oneToken);
    });

    it("should successfully transfer tokens", async () => {
      await mrGreedyToken.approve(OWNER, 100 * oneToken);

      await mrGreedyToken.transferFrom(OWNER, DUDE, 7 * oneToken);

      assert.equal(await mrGreedyToken.balanceOf(OWNER), 93 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(DUDE), 0 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(TREASURY), 7 * oneToken);

      await mrGreedyToken.transferFrom(OWNER, DUDE, 93 * oneToken);

      assert.equal(await mrGreedyToken.balanceOf(OWNER), 0 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(DUDE), 83 * oneToken);
      assert.equal(await mrGreedyToken.balanceOf(TREASURY), 17 * oneToken);
    });
  });
});
