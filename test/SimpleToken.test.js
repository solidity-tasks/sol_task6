const { assert } = require("chai");
const { accounts } = require("../scripts/utils/utils");
const truffleAssert = require("truffle-assertions");
const SimpleToken = artifacts.require("SimpleToken");

describe("SimpleToken", () => {
  let OWNER;
  let DUDE;

  let simpleToken;

  before("setup", async () => {
    OWNER = await accounts(0);
    DUDE = await accounts(1);
  });

  describe("constructor", () => {
    it("should set all variables", async () => {
      simpleToken = await SimpleToken.new("SimpleToken", "ST");

      assert.equal(await simpleToken.name(), "SimpleToken");
      assert.equal(await simpleToken.symbol(), "ST");
    });
  });

  describe("mint()", () => {
    beforeEach("setup", async () => {
      simpleToken = await SimpleToken.new("SimpleToken", "ST");
    });

    it("only owner should mint", async () => {
      await truffleAssert.reverts(simpleToken.mint(DUDE, "1", { from: DUDE }), "Ownable: caller is not the owner");
    });

    it("should successfully mint", async () => {
      await simpleToken.mint(OWNER, "1");

      assert.equal(await simpleToken.totalSupply(), "1");
      assert.equal(await simpleToken.balanceOf(OWNER), "1");
    });
  });

  describe("burn()", () => {
    beforeEach("setup", async () => {
      simpleToken = await SimpleToken.new("SimpleToken", "ST");

      await simpleToken.mint(DUDE, "1");
    });

    it("should successfully burn", async () => {
      await simpleToken.burn("1", { from: DUDE });

      assert.equal(await simpleToken.balanceOf(DUDE), "0");
    });
  });
});
