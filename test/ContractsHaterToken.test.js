const { assert } = require("chai");
const { accounts } = require("../scripts/utils/utils");
const truffleAssert = require("truffle-assertions");
const ContractsHaterToken = artifacts.require("ContractsHaterToken");

describe("ContractsHaterToken", () => {
  let OWNER;
  let DUDE;

  let contractsHaterToken;

  before("setup", async () => {
    OWNER = await accounts(0);
    DUDE = await accounts(1);
  });

  describe("addToWhitelist()", () => {
    beforeEach("setup", async () => {
      contractsHaterToken = await ContractsHaterToken.new("ContractsHaterToken", "CHT");
    });

    it("only owner should add to whitelist", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.addToWhitelist(contractsHaterToken.address, { from: DUDE }),
        "Ownable: caller is not the owner"
      );
    });

    it("should not add an address that is not a contract", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.addToWhitelist(OWNER),
        "ContractsHaterToken: the address is not a contract"
      );
    });

    it("should successfully add to whitelist", async () => {
      await contractsHaterToken.addToWhitelist(contractsHaterToken.address);

      assert.isTrue(await contractsHaterToken.isWhitelisted(contractsHaterToken.address));
    });
  });

  describe("removeFromWhitelist()", () => {
    beforeEach("setup", async () => {
      contractsHaterToken = await ContractsHaterToken.new("ContractsHaterToken", "CHT");

      await contractsHaterToken.addToWhitelist(contractsHaterToken.address);
    });

    it("only owner should remove from whitelist", async () => {
      await truffleAssert.reverts(
        contractsHaterToken.removeFromWhitelist(contractsHaterToken.address, { from: DUDE }),
        "Ownable: caller is not the owner"
      );
    });

    it("should successfully remove from whitelist", async () => {
      await contractsHaterToken.removeFromWhitelist(contractsHaterToken.address);

      assert.isFalse(await contractsHaterToken.isWhitelisted(contractsHaterToken.address));
    });
  });

  describe("_beforeTokenTransfer()", () => {
    beforeEach("setup", async () => {
      contractsHaterToken = await ContractsHaterToken.new("ContractsHaterToken", "CHT");
    });

    it("should successfully mint", async () => {
      await contractsHaterToken.mint(DUDE, "1");

      assert.equal(await contractsHaterToken.balanceOf(DUDE), "1");
    });

    it("should successfully burn", async () => {
      await contractsHaterToken.mint(DUDE, "3");
      await contractsHaterToken.burn("2", { from: DUDE });

      assert.equal(await contractsHaterToken.balanceOf(DUDE), "1");
    });

    it("should not transfer tokens if address is contract that is not whitelisted", async () => {
      await contractsHaterToken.mint(OWNER, "1");

      await truffleAssert.reverts(
        contractsHaterToken.transfer(contractsHaterToken.address, "1"),
        "ContractsHaterToken: the contract must be whitelisted"
      );
    });

    it("should successfully transfer tokens", async () => {
      await contractsHaterToken.mint(OWNER, "2");

      await contractsHaterToken.transfer(DUDE, "1");

      assert.equal(await contractsHaterToken.balanceOf(OWNER), "1");
      assert.equal(await contractsHaterToken.balanceOf(DUDE), "1");

      await contractsHaterToken.addToWhitelist(contractsHaterToken.address);
      await contractsHaterToken.transfer(contractsHaterToken.address, "1");

      assert.equal(await contractsHaterToken.balanceOf(OWNER), "0");
      assert.equal(await contractsHaterToken.balanceOf(contractsHaterToken.address), "1");
    });
  });
});
